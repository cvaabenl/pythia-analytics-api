from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.openapi import Schema, TYPE_STRING, TYPE_ARRAY
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

import pandas as pd
from datetime import datetime, timedelta, date

from common.serializers import TargetSerializer, QuerySerializer, AnnotationSerializer, KeyValueSerializer
from sparksays.models import SparkSays
from sparksays.serializers import SparkSaysSerializer


# Create your views here.
def index(request):

    today = date.today()
    date_delta = timedelta(days=5)
    queryset = SparkSays.objects.filter(date__range=(today - date_delta, today)).order_by('timestamp').values()
    df = pd.DataFrame(list(queryset))

    subj = {}

    if not df.empty:
        subjects = df["subject"].unique().tolist()
        for s in subjects:
            sub_df = df[df['subject'] == s]
            ids = sub_df['ssid'].unique().tolist()
            iddata = {}
            for i in ids:
                l = df[df['ssid'] == i]['count'].tolist()
                l.reverse()
                iddata[i] = l
            subj[s] = iddata

    return render(request, 'sparksays/index.html', {'subjects': subj})


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class Query(APIView):
    def get_data(self, request):
        """
        List all code snippets, or create a new snippet.
        """
        req = request.data

        range_from = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        range_to = (datetime.now()).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        if 'range' in req:
            if 'from' in req['range']:
                range_from = req['range']['from']
            if 'to' in req['range']:
                range_to = req['range']['to']

        queryset = SparkSays.objects.filter(date__range=(range_from, range_to)).order_by('timestamp').values()
        df = pd.DataFrame(list(queryset))

        subjects = df["subject"].unique().tolist()

        target_type = 'timeserie'
        targets = []
        for t in req['targets']:
            target = t['target']
            if target in subjects:
                targets += df[df['subject'] == target]['ssid'].unique().tolist()
            else:
                targets.append(target)
            target_type = t['type']

        df = df[df['ssid'].isin(targets)]
        df = df[(df['timestamp'] >= range_from) & (df['timestamp'] <= range_to)]
        df = df.rename(columns={'ssid': 'target'})

        response = []

        if target_type == 'timeserie':
            if df.empty:
                for t in targets:
                    response.append({'target': '%s' % (t), 'datapoints': []})

            else:
                df['datapoint'] = df.apply(lambda row: [row['count'], row['timestamp']], axis=1)
                result_df = df.groupby('target')['datapoint'].apply(list).reset_index(name='datapoints')

                response = list(result_df.to_dict(orient='index').values())

        else:  # table
            if df.empty:
                for t in targets:
                    response.append({'type': 'table', 'columns': ['value', 'timestamp'], 'rows': []})

            else:
                for t in targets:
                    result_df = df[df['target'] == t].drop(['target', 'ssid'], axis=1)
                    df_dict = result_df.to_dict(orient='split')

                    response.append({'type': 'table', 'columns': df_dict['columns'], 'rows': df_dict['data']})

        return response

    @swagger_auto_schema(request_body=QuerySerializer, operation_id="kpi-tracking_query")
    def post(self, request):
        return Response(self.get_data(request))


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class Search(APIView):
    def get_data(self, request):
        today = date.today()
        date_delta = timedelta(days=2)
        queryset = SparkSays.objects.filter(date__range=(today - date_delta, today)).values()
        df = pd.DataFrame(list(queryset))

        ids = df["ssid"].unique().tolist()

        return df["subject"].unique().tolist() + ids

    @swagger_auto_schema(responses={
        200: Schema(type=TYPE_ARRAY, items=Schema(type=TYPE_STRING))
    })
    def get(self, request):
        return Response(self.get_data(request))

    @swagger_auto_schema(request_body=TargetSerializer, responses={
        200: Schema(type=TYPE_ARRAY, items=Schema(type=TYPE_STRING))
    })
    def post(self, request):
        return Response(self.get_data(request))


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class Annotations(APIView):
    def get_data(self, request):
        """
        List all code snippets, or create a new snippet.
        """
        return []

    def get(self, request):
        return Response(self.get_data(request))

    @swagger_auto_schema(request_body=AnnotationSerializer)
    def post(self, request):
        return Response(self.get_data(request))


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class TagKeys(APIView):
    def get_data(self, request):
        """
        List all code snippets, or create a new snippet.
        """
        return []

    def get(self, request):
        return Response(self.get_data(request))

    def post(self, request):
        return Response(self.get_data(request))


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class TagValues(APIView):
    def get_data(self, request):
        """
        List all code snippets, or create a new snippet.
        """
        return []

    def get(self, request):
        return Response(self.get_data(request))

    @swagger_auto_schema(request_body=KeyValueSerializer)
    def post(self, request):
        return Response(self.get_data(request))


class SparkSaysViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows KPITracking to be viewed.
    """
    queryset = SparkSays.objects.all().order_by('-subject')
    serializer_class = SparkSaysSerializer
    # permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'ssid': ["in", "exact"],
        'subject': ["exact"],
        'date': ['gte', 'lte', 'exact', 'gt', 'lt']
    }
