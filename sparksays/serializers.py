from sparksays.models import SparkSays
from rest_framework import serializers


class SparkSaysSerializer(serializers.ModelSerializer):
    class Meta:
        model = SparkSays
        fields = '__all__'
