from django.urls import path, include
from rest_framework import routers

from sparksays import views

router = routers.DefaultRouter()
router.register(r'api', views.SparkSaysViewSet)

urlpatterns = [
    path('', views.index, name='sparksays_index'),
    path('', include(router.urls)),
    path('grafana/search', views.Search.as_view()),
    path('grafana/query', views.Query.as_view()),
    path('grafana/annotations', views.Annotations.as_view()),
    path('grafana/tag-keys', views.TagKeys.as_view()),
    path('grafana/tag-values', views.TagValues.as_view())
]