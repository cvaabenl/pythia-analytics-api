from django.db import models


class SparkSays(models.Model):
    """
    root
    |-- type: string (nullable = true)
    |-- value: double (nullable = false)
    |-- timestamp: timestamp (nullable = false)
    |-- id: string (nullable = false)
    """
    ssid = models.CharField(max_length=100,default="")
    subject = models.CharField(max_length=100)
    count = models.FloatField()
    date = models.DateField()
    timestamp = models.DateTimeField()

    class Meta:
        unique_together = (("ssid", "date"),)
        db_table = 'spark_says'
