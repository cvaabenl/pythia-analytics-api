from django.urls import path, include
from recommendation import views, cooling_views, capacitor_status_views
from rest_framework import routers


capacitor_router = routers.DefaultRouter()
capacitor_router.register(r'api', capacitor_status_views.CapacitorStatusViewSet)

cooling_router = routers.DefaultRouter()
cooling_router.register(r'api', cooling_views.CoolingAlarmRecommendationViewSet)

urlpatterns = [

    path('', views.index, name='recommendation_index'),
    path('cooling/index', cooling_views.index, name='cooling_index'),
    path('cooling/', include(cooling_router.urls)),
    # path('cooling/<str:device_family>/<str:device_version>', cooling_views.Lookup.as_view()),
    path('capacitor_status/index', capacitor_status_views.index, name='capacitor_status_index'),
    path('capacitor_status/', include(capacitor_router.urls)),
    # path(r'capacitor_status/', capacitor_status_views.Lookup.as_view())
]
