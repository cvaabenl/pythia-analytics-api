from rest_framework import serializers
from recommendation.models import CapacitorStatus, CoolingAlarmRecommendation


class CoolingAlarmRecommendationSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoolingAlarmRecommendation
        fields = "__all__"


class CapacitorStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = CapacitorStatus
        fields = '__all__'
