from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from django.db.models import Q

from recommendation.models import CapacitorStatus
from recommendation.serializers import CapacitorStatusSerializer

import pandas as pd


def index(request):

    cutoff = 24
    queryset = CapacitorStatus.objects.filter(Q(c0__lte=cutoff) | Q(c1__lte=cutoff)).order_by('timestamp').values()
    df = pd.DataFrame(list(queryset))

    data = df.to_dict(orient='index')

    return render(request, 'recommendation/index_capacitor.html', {'data': data})


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
# class Lookup(APIView):
#     def get_data(self, request):
#         """
#         List all code snippets, or create a new snippet.
#         """
#         params = request.query_params
#
#         df = read_data().fillna(0)
#         if 'cutoff' in params:
#             cutoff = int(params['cutoff'])
#             s0 = df['c0'] < cutoff
#             s1 = df['c1'] < cutoff
#             df = df[s0 + s1]
#
#         # df = df.filter(items=['alarm_id', 'shutdown', 'recommended_action'])
#         response = list(df.to_dict(orient='index').values())
#
#         return response
#
#     @swagger_auto_schema(operation_id="lookup_recommendation", responses={
#         200: Schema(
#             type=TYPE_ARRAY,
#             items=Schema(
#                 type=TYPE_OBJECT,
#                 properties={
#                     'alarm_id': Schema(type=TYPE_STRING),
#                     'shutdown': Schema(type=TYPE_STRING),
#                     'recommended_action': Schema(type=TYPE_STRING)}
#                 )
#             )
#     })
#     def get(self, request):
#         return Response(self.get_data(request))


class CapacitorStatusViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows KPITracking to be viewed.
    """
    queryset = CapacitorStatus.objects.all()
    serializer_class = CapacitorStatusSerializer
    # permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'c0': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'c1': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'date': ['gte', 'lte', 'exact', 'gt', 'lt']
    }
