from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from recommendation.models import CoolingAlarmRecommendation
from recommendation.serializers import CoolingAlarmRecommendationSerializer


def index(request):
    return render(request, 'recommendation/index_cooling.html')


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
# class Lookup(APIView):
#     def get_data(self, request, family, version):
#         """
#         List all code snippets, or create a new snippet.
#         """
#         req = request.data
#
#         if ('alarm_id' in req) and (isinstance(req['alarm_id'], dict)):
#             if ('alarm_text' in req['alarm_id']) and ('alarm_severity' in req['alarm_id']):
#                 df = read_data()
#
#                 df = df[df['device_family'] == family]
#                 df = df[df['device_version'] == version]
#
#                 df = df[df['active_text'] == req['alarm_id']['alarm_text']]
#                 df = df[df['severity'] == req['alarm_id']['alarm_severity']]
#
#                 df = df.filter(items=['alarm_id', 'shutdown', 'recommended_action'])
#
#                 response = list(df.to_dict(orient='index').values())
#
#                 return response
#
#         return []
#
#     @swagger_auto_schema(request_body=CoolingAlarmSerializer, operation_id="lookup_recommendation", responses={
#         # 200: TimeserieSerializer(many=True)
#         200: Schema(
#             type=TYPE_ARRAY,
#             items=Schema(
#                 type=TYPE_OBJECT,
#                 properties={
#                     'alarm_id': Schema(type=TYPE_STRING),
#                     'shutdown': Schema(type=TYPE_STRING),
#                     'recommended_action': Schema(type=TYPE_STRING)}
#                 )
#             )
#     })
#     def post(self, request, device_family, device_version):
#         return Response(self.get_data(request, device_family, device_version))


class CoolingAlarmRecommendationViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows KPITracking to be viewed.
    """
    queryset = CoolingAlarmRecommendation.objects.all()
    serializer_class = CoolingAlarmRecommendationSerializer
    # permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'device_family': ['in', 'exact'],
        'device_version': ['in', 'exact'],
        'severity': ['in', 'exact'],
        'active_text': ['exact']
    }
