from django.db import models


# Recommendation models
class CoolingAlarmRecommendation(models.Model):
    device_class = models.CharField(max_length=25)
    device_type = models.CharField(max_length=25)
    device_family = models.CharField(max_length=25)
    device_version = models.CharField(max_length=25)
    alarm_id = models.CharField(max_length=25)
    active_text = models.CharField(max_length=100)
    inactive_text = models.CharField(max_length=100)
    severity = models.CharField(max_length=25)
    shutdown = models.CharField(max_length=25)
    category = models.CharField(max_length=25)
    active_description = models.CharField(max_length=150)
    inactive_description = models.CharField(max_length=150)
    recommended_action = models.CharField(max_length=1000)

    class Meta:
        unique_together = (("device_class", "device_type", "device_family", "device_version", "alarm_id"),)
        db_table = 'cooling_alarm_recommendation'


class CapacitorStatus(models.Model):
    dceserial = models.UUIDField()
    organizationid = models.UUIDField()
    device_id = models.CharField(max_length=150)
    location = models.CharField(max_length=100)
    phase = models.IntegerField()
    c0 = models.IntegerField()
    c1 = models.IntegerField()
    assetid = models.UUIDField()
    licensetype = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    skunumber = models.CharField(max_length=50)
    serialnumber = models.CharField(max_length=50)
    firmwareversion = models.CharField(max_length=50)
    hardwareversion = models.CharField(max_length=50)
    date = models.DateField()
    timestamp = models.DateTimeField()

    class Meta:
        unique_together = (("device_id", "location"),)
        db_table = 'capacitor_status'
