"""
    Helper functions that are used across different sub-apps
"""
from django.contrib.auth import authenticate, login
import re
import base64


def pre_authenticate(request):
    """
        Log in user if basic credentials are provided in the request

        Usage:
        pre_authenticate(request)
            if not request.user.is_authenticated:
                return HttpResponseNotFound("")
    """
    p = re.compile("Basic ([\w=]+)")

    if 'Authorization' in request.headers:
        auth = re.match(p, request.headers['Authorization'])
        cred = base64.b64decode(auth.groups()[0]).decode("utf-8").split(":", maxsplit=1)
        if len(cred) == 2:
            user = authenticate(request, username=cred[0], password=cred[1])
            if user is not None:
                login(request, user)
    else:
        print("No Authorization")
