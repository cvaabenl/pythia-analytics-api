from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from common.models import Target, Query, Annotation, KeyValue, Timeserie
from drf_yasg import openapi


class TargetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Target
        fields = '__all__'


class RangeField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "properties": {
                "from": openapi.Schema(
                    type=openapi.TYPE_STRING,
                    format=openapi.FORMAT_DATETIME
                ),
                "to": openapi.Schema(
                    type=openapi.TYPE_STRING,
                    format=openapi.FORMAT_DATETIME
                ),
                "raw": {
                    "type": openapi.TYPE_OBJECT,
                    "properties": {
                        "from": openapi.Schema(
                            type=openapi.TYPE_STRING,
                        ),
                        "to": openapi.Schema(
                            type=openapi.TYPE_STRING,
                        ),
                    }
                }
            },
            "required": ["from", "to"],
        }


class SimpleRangeField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "properties": {
                "from": openapi.Schema(
                    type=openapi.TYPE_STRING,
                    format=openapi.FORMAT_DATETIME
                ),
                "to": openapi.Schema(
                    type=openapi.TYPE_STRING,
                    format=openapi.FORMAT_DATETIME
                )
            },
            "required": ["from", "to"],
        }


class RangeRawField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "title": "Raw Range",
            "properties": {
                "from": openapi.Schema(
                    title="From date",
                    type=openapi.TYPE_STRING
                ),
                "to": openapi.Schema(
                    title="To date",
                    type=openapi.TYPE_STRING
                )
            },
            "required": ["from", "to"],
        }


class TargetsField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_ARRAY,
            "items": {
                "type": openapi.TYPE_OBJECT,
                "title": "Target",
                "properties": {
                    "target": openapi.Schema(
                        type=openapi.TYPE_STRING
                    ),
                    "refId": openapi.Schema(
                        type=openapi.TYPE_STRING
                    ),
                    "type": openapi.Schema(
                        type=openapi.TYPE_STRING,
                        enum=['timeseries', 'table']
                    )
                }
            }
        }


class AdhocFiltersField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_ARRAY,
            "items": {
                "type": openapi.TYPE_OBJECT,
                "title": "Adhoc Filter",
                "properties": {
                    "key": openapi.Schema(
                        type=openapi.TYPE_STRING
                    ),
                    "operator": openapi.Schema(
                        type=openapi.TYPE_STRING,
                        enum=['=']
                    ),
                    "value": openapi.Schema(
                        type=openapi.TYPE_STRING
                    )
                }
            }
        }


class AnnotationField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "title": "Annotation",
            "properties": {
                "name": openapi.Schema(
                    type=openapi.TYPE_STRING
                ),
                "datasource": openapi.Schema(
                    type=openapi.TYPE_STRING
                ),
                "iconColor": openapi.Schema(
                    type=openapi.TYPE_STRING
                ),
                "enabled": openapi.Schema(
                    type=openapi.TYPE_BOOLEAN
                ),
                "query": openapi.Schema(
                    type=openapi.TYPE_STRING
                )
            },
            "required": [],
        }


class QuerySerializer(ModelSerializer):
    class Meta:
        model = Query
        fields = "__all__"

    range = RangeField()
    rangeRaw = RangeRawField()
    targets = TargetsField()
    adhocFilters = AdhocFiltersField()


class AnnotationSerializer(ModelSerializer):
    class Meta:
        model = Annotation
        fields = "__all__"

    range = SimpleRangeField()
    rangeRaw = RangeRawField()
    annotation = AnnotationField()


class KeyValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = KeyValue
        fields = '__all__'


class DatapointsField(serializers.JSONField):
    class Meta:
        swagger_schema_fields = {
            "type": openapi.TYPE_ARRAY,
            "items": {
                "type": openapi.TYPE_ARRAY,
                "items": {
                    "type": openapi.TYPE_INTEGER
                }
            }
        }


class TimeserieSerializer(ModelSerializer):
    class Meta:
        model = Timeserie
        fields = ['target', 'datapoints']

        datapoints = DatapointsField()
