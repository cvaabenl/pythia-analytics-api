from django.db import models


# Create your models here.
class Target(models.Model):
    target = models.CharField(max_length=100)


class Timeserie(models.Model):
    target = models.CharField(max_length=100)
    datapoints = models.JSONField()


class Query(models.Model):
    panelId = models.CharField(max_length=100)
    range = models.JSONField()
    rangeRaw = models.JSONField()
    interval = models.CharField(max_length=20)
    intervalMs = models.BigIntegerField()
    targets = models.JSONField()
    adhocFilters = models.JSONField()
    format = models.CharField(max_length=10)
    maxDataPoints = models.IntegerField()


class Annotation(models.Model):
    range = models.JSONField()
    rangeRaw = models.JSONField()
    annotation = models.JSONField()


class KeyValue(models.Model):
    key = models.CharField(max_length=20)
