from django.shortcuts import render


def index(request):
    return render(request, 'common/index.html')


def swagger_inline(request):
    return render(request, 'common/swagger.html')
