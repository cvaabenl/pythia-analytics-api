

class DatabaseRouter:
    """
    Database Router to help route model read/writes to the correct databases
    """
    route_app_labels = {'sparksays': 'sparksays', 'kpi_tracking': 'kpitracking', 'recommendation': 'recommendation'}

    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.route_app_labels.keys():
            return self.route_app_labels[model._meta.app_label]
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.route_app_labels.keys():
            return self.route_app_labels[model._meta.app_label]
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if (
                obj1._meta.app_label in self.route_app_labels.keys() or
                obj2._meta.app_label in self.route_app_labels.keys()
        ):
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label in self.route_app_labels.keys():
            return db == self.route_app_labels[app_label]
        return False
