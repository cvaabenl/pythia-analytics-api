from kpi_tracking.models import KPITracking
from rest_framework import serializers


class KPITrackingSerializer(serializers.ModelSerializer):
    class Meta:
        model = KPITracking
        fields = '__all__'
