from django.urls import path, include
from kpi_tracking import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'api', views.KPITrackingViewSet)

urlpatterns = [
    path('', views.index, name='kpi_tracking_index'),
    path('', include(router.urls)),
    path('grafana/search', views.Search.as_view()),
    path('grafana/query', views.Query.as_view()),
    path('grafana/annotations', views.Annotations.as_view()),
    path('grafana/tag-keys', views.TagKeys.as_view()),
    path('grafana/tag-values', views.TagValues.as_view())
]