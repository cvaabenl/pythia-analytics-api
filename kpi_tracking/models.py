from django.db import models


class KPITracking(models.Model):
    """
    root
    |-- type: string (nullable = true)
    |-- value: double (nullable = false)
    |-- timestamp: timestamp (nullable = false)
    |-- id: string (nullable = false)
    """
    type = models.CharField(max_length=100)
    date = models.DateField()
    value = models.FloatField()
    timestamp = models.DateTimeField()

    class Meta:
        unique_together = (("type", "date"),)
        db_table = 'kpi_tracking'
