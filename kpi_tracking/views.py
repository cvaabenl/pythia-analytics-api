from django.shortcuts import render
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.openapi import Schema, TYPE_STRING, TYPE_ARRAY, TYPE_OBJECT, TYPE_NUMBER
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

import pandas as pd
from datetime import datetime, timedelta, date


from common.serializers import TargetSerializer, QuerySerializer, AnnotationSerializer, KeyValueSerializer
from kpi_tracking.models import KPITracking
from kpi_tracking.serializers import KPITrackingSerializer


# Create your views here.
def index(request):
    return render(request, 'kpi_tracking/index.html')


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class Query(APIView):
    def get_data_v2(self, request):
        """
        List all code snippets, or create a new snippet.
        """
        req = request.data

        target_type = 'timeserie'
        targets = []
        for t in req['targets']:
            targets.append(t['target'])
            target_type = t['type']

        range_from = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        range_to = (datetime.now()).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        if 'range' in req:
            if 'from' in req['range']:
                range_from = req['range']['from']
            if 'to' in req['range']:
                range_to = req['range']['to']

        df = pd.DataFrame(list(KPITracking.objects.filter(type__in=targets).filter(timestamp__range=(range_from, range_to)).values()))
        df = df.rename(columns={'type': 'target'})
        response = []

        if target_type == 'timeserie':
            if df.empty:
                for t in targets:
                    response.append({'target': '%s' % (t), 'datapoints': []})

            else:
                df['datapoint'] = df.apply(lambda row: [row['value'], row['timestamp']], axis=1)
                result_df = df.groupby('target')['datapoint'].apply(list).reset_index(name='datapoints')

                response = list(result_df.to_dict(orient='index').values())

        else:  # table
            if df.empty:
                for t in targets:
                    response.append({'type': 'table', 'columns': ['value', 'timestamp'], 'rows': []})

            else:
                for t in targets:
                    result_df = df[df['target'] == t].drop(['target', 'id'], axis=1)
                    df_dict = result_df.to_dict(orient='split')

                    response.append({'type': 'table', 'columns': df_dict['columns'], 'rows': df_dict['data']})

        return response

    @swagger_auto_schema(request_body=QuerySerializer, operation_id="kpi-tracking_query", responses={
        # 200: TimeserieSerializer(many=True)
        200: Schema(
            type=TYPE_OBJECT,
            properties={
                'target': Schema(type=TYPE_STRING),
                'datapoints': Schema(
                    type=TYPE_ARRAY,
                    items=Schema(type=TYPE_ARRAY, items=Schema(type=TYPE_NUMBER)))})
    })
    def post(self, request):
        return Response(self.get_data_v2(request))


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class Search(APIView):
    def get_data_v2(self):
        today = date.today()
        date_delta = timedelta(days=2)
        return list(KPITracking.objects.filter(date__range=(today - date_delta, today)).distinct('type').values_list('type', flat=True))

    @swagger_auto_schema(responses={
        200: Schema(type=TYPE_ARRAY, items=Schema(type=TYPE_STRING))
    })
    def get(self, request):
        return Response(self.get_data_v2())

    @swagger_auto_schema(request_body=TargetSerializer, responses={
        200: Schema(type=TYPE_ARRAY, items=Schema(type=TYPE_STRING))
    })
    def post(self, request):
        return Response(self.get_data_v2())


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class Annotations(APIView):
    def get_data(self):
        """
        List all code snippets, or create a new snippet.
        """
        return []

    def get(self, request):
        return Response(self.get_data())

    @swagger_auto_schema(request_body=AnnotationSerializer)
    def post(self, request):
        return Response(self.get_data())


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class TagKeys(APIView):
    def get_data(self, request):
        """
        List all code snippets, or create a new snippet.
        """
        return []

    def get(self, request):
        return Response(self.get_data(request))

    def post(self, request):
        return Response(self.get_data(request))


# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
class TagValues(APIView):
    def get_data(self, request):
        """
        List all code snippets, or create a new snippet.
        """
        return []

    def get(self, request):
        return Response(self.get_data(request))

    @swagger_auto_schema(request_body=KeyValueSerializer)
    def post(self, request):
        return Response(self.get_data(request))


class KPITrackingViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows KPITracking to be viewed.
    """
    queryset = KPITracking.objects.all().order_by('-type')
    serializer_class = KPITrackingSerializer
    # permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'type': ["in", "exact"],
        'date': ['gte', 'lte', 'exact', 'gt', 'lt']
    }
