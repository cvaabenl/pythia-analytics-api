from django.apps import AppConfig


class KpiTrackingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kpi_tracking'
